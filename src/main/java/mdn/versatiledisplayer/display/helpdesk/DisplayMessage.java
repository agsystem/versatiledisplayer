/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.versatiledisplayer.display.helpdesk;

import java.util.Map;

/**
 *
 * @author mbahbejo
 */
public class DisplayMessage {
    private String displayer;
    private Map<String, String> params;

    public DisplayMessage(String displayer, Map<String, String> params) {
        this.displayer = displayer;
        this.params = params;
    }

    public String getDisplayer() {
        return displayer;
    }

    public void setDisplayer(String displayer) {
        this.displayer = displayer;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }    
    
}
