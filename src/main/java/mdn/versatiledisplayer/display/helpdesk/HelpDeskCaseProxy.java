/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package mdn.versatiledisplayer.display.helpdesk;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public class HelpDeskCaseProxy {

  private String srcWorkUnit;

  private Date submitDate;

  private String destWorkUnit;

  private String content;

  public HelpDeskCaseProxy() {
  }

  public HelpDeskCaseProxy(String srcWorkUnit, String destWorkUnit, Date submitDate, String content) {
    this.srcWorkUnit = srcWorkUnit;
    this.submitDate = submitDate;
    this.destWorkUnit = destWorkUnit;
    this.content = content;
  }

  public String getSrcWorkUnit() {
//    return srcWorkUnit.toUpperCase();
    return srcWorkUnit.toUpperCase() + "/" + destWorkUnit.toUpperCase();
  }

  public void setSrcWorkUnit(String srcWorkUnit) {
    this.srcWorkUnit = srcWorkUnit;
  }

  public String getDestWorkUnit() {
    return destWorkUnit.toUpperCase();
  }

  public void setDestWorkUnit(String destWorkUnit) {
    this.destWorkUnit = destWorkUnit;
  }

  public Date getSubmitDate() {
    return submitDate;
  }

  public void setSubmitDate(Date submitDate) {
    this.submitDate = submitDate;
  }

  public String getContent() {
    return content.toUpperCase();
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getStrSubmitDate() {
    DateFormat sdf = new SimpleDateFormat("MMMMM d, yyyy HH:mm:ss");
    if (submitDate != null) {
      return sdf.format(submitDate);
    } else {
      return null;
    }
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 67 * hash + Objects.hashCode(this.srcWorkUnit);
    hash = 67 * hash + Objects.hashCode(this.submitDate);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final HelpDeskCaseProxy other = (HelpDeskCaseProxy) obj;
    if (!Objects.equals(this.srcWorkUnit, other.srcWorkUnit)) {
      return false;
    }
    if (!Objects.equals(this.submitDate, other.submitDate)) {
      return false;
    }
    return true;
  }
}
