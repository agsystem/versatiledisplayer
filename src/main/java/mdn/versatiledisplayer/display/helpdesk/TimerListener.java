/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.versatiledisplayer.display.helpdesk;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public interface TimerListener {
  public void onTick();
}
