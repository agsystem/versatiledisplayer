package mdn.versatiledisplayer.display.helpdesk;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.media.AudioClip;
import javafx.util.Callback;
import mdn.iotlib.comm.message.Message;
import mdn.versatiledisplayer.Displayer;
import mdn.versatiledisplayer.common.Device;
import mdn.versatiledisplayer.common.DisplayController;
import mdn.versatiledisplayer.display.linecounter.LineCounterController;

public class HelpDeskController implements DisplayController {

    @FXML
    private TableView<HelpDeskCaseProxy> tblView;

    @FXML
    private TableColumn<HelpDeskCaseProxy, String> colFrom;

    @FXML
    private TableColumn<HelpDeskCaseProxy, String> colContent;

    @FXML
    private TableColumn<HelpDeskCaseProxy, Date> colTimeElapse;

    private ListProperty listProperty;

    private Device device;

    public HelpDeskController() {
        tblView = new TableView<>();
    }

    @FXML
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        ObservableList<HelpDeskCaseProxy> listCaseData = FXCollections.observableArrayList();

        listProperty = new SimpleListProperty(listCaseData);

        tblView.itemsProperty().bind(listProperty);
        tblView.setMaxHeight(Displayer.SCREEN_HEIGHT);

        colFrom.setPrefWidth(Displayer.SCREEN_WIDTH * 15 / 100);
        colFrom.setCellValueFactory(new PropertyValueFactory<HelpDeskCaseProxy, String>("srcWorkUnit"));
        colFrom.setCellFactory(new Callback<TableColumn<HelpDeskCaseProxy, String>, TableCell<HelpDeskCaseProxy, String>>() {

            @Override
            public TableCell<HelpDeskCaseProxy, String> call(TableColumn<HelpDeskCaseProxy, String> param) {
                return new HelpDeskCaseCenteredCell();
            }
        });
        
        colContent.setPrefWidth(Displayer.SCREEN_WIDTH * 60 / 100);
        colContent.setCellValueFactory(new PropertyValueFactory<HelpDeskCaseProxy, String>("content"));
        colContent.setCellFactory(new Callback<TableColumn<HelpDeskCaseProxy, String>, TableCell<HelpDeskCaseProxy, String>>() {

            @Override
            public TableCell<HelpDeskCaseProxy, String> call(TableColumn<HelpDeskCaseProxy, String> param) {
                return new WHelpDeskCaseCell();
            }

        });

        colTimeElapse.setPrefWidth(Displayer.SCREEN_WIDTH * 25 / 100);
        colTimeElapse.setCellValueFactory(new PropertyValueFactory<HelpDeskCaseProxy, Date>("submitDate"));

        colTimeElapse.setCellFactory(new Callback<TableColumn<HelpDeskCaseProxy, Date>, TableCell<HelpDeskCaseProxy, Date>>() {

            @Override
            public TableCell<HelpDeskCaseProxy, Date> call(TableColumn<HelpDeskCaseProxy, Date> param) {
                return new HelpDeskCaseTimerCell();
            }
        });

    }

    public void addCase(final HelpDeskCaseProxy hdCase) {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                if (listProperty.isEmpty()) {
                    listProperty.add(hdCase);
                } else {
                    HelpDeskCaseProxy proxy = (HelpDeskCaseProxy) listProperty.getValue().get(0);
                    if (hdCase.getSubmitDate().after(proxy.getSubmitDate())) {
                        listProperty.add(0, hdCase);
                        tblView.refresh();
                        URL resource = getClass().getResource("/audio/emergency.wav");
                        final AudioClip clip = new AudioClip(resource.toString());
                        clip.play();
                    } else {
                        listProperty.add(hdCase);
                    }
                }
            }
        });
    }

    public void updateCase(final HelpDeskCaseProxy hdCase) {

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                int idxUpdated = listProperty.indexOf(hdCase);
                listProperty.remove(idxUpdated);
                listProperty.add(idxUpdated, hdCase);
                tblView.refresh();
            }
        });

    }

    public void removeCase(final HelpDeskCaseProxy hdCase) {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                listProperty.remove(hdCase);
                tblView.refresh();
            }
        });
    }

    public void onNewCase(HelpDeskCaseProxy newCase) {
        addCase(newCase);
    }

    public void onCompletedCase(HelpDeskCaseProxy completedCase) {
        removeCase(completedCase);
    }

    public void onDeletedCase(HelpDeskCaseProxy deletedCase) {
        removeCase(deletedCase);
    }

    public void onUpdateCase(HelpDeskCaseProxy updatedCase) {
        updateCase(updatedCase);
    }

    @Override
    public void process(Message message) {
        Gson gson = new Gson();
        Type collectionType = new TypeToken<Map<String, String>>(){}.getType();
        Map<String, String> params =  gson.fromJson(message.getString(), collectionType);
        HelpDeskCaseProxy proxy = new HelpDeskCaseProxy();
        proxy.setSrcWorkUnit(params.get("source"));
        proxy.setDestWorkUnit(params.get("destination"));
//        DateFormat sdf = new SimpleDateFormat("MMMMM d, yyyy HH:mm:ss");
        proxy.setSubmitDate(new Date(Long.parseLong(params.get("date"))));
        proxy.setContent(params.get("content"));
        switch (params.get("type")) {
            case "new":
                onNewCase(proxy);
                break;
            case "solve":
                onCompletedCase(proxy);
                break;
            case "update":
                onUpdateCase(proxy);
                break;
            case "delete":
                onDeletedCase(proxy);
                break;
            default:
                break;
        }
    }


    @Override
    public void onOpen() {
        Message msg = new Message(getDisplayName().getBytes().length);
        msg.setSource(getDevice().getId());
        msg.setFlag(-1);
        msg.putString(getDisplayName());
        try {
            getDevice().send(msg);
            MasterTimer timer = MasterTimer.getInstance();
            timer.start();
        } catch (Exception ex) {
            Logger.getLogger(HelpDeskController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onHide() {
        MasterTimer timer = MasterTimer.getInstance();
        timer.clearListener();
        timer.stop();
    }
    
    @Override
    public void onClose() {
        MasterTimer timer = MasterTimer.getInstance();
        timer.clearListener();
        timer.stop();
    }

    @Override
    public String getDisplayName() {
        return "HelpDesk";
    }

    @Override
    public void setDevice(Device device) {
        this.device = device;
    }

    @Override
    public Device getDevice() {
        return device;
    }
}
