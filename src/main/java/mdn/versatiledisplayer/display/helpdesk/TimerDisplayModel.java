/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.versatiledisplayer.display.helpdesk;

import java.util.Calendar;
import java.util.Date;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public class TimerDisplayModel implements TimerListener {

  private static final int DAYS;

  private static final int HOURS;

  private static final int MINUTES;

  static {
    DAYS = 24 * 60 * 60;
    HOURS = 60 * 60;
    MINUTES = 60;
  }

  public StringProperty daysMajor = new SimpleStringProperty("0");
  public StringProperty daysMinor = new SimpleStringProperty("0");
  public StringProperty hoursMajor = new SimpleStringProperty("0");
  public StringProperty hoursMinor = new SimpleStringProperty("0");
  public StringProperty minutesMajor = new SimpleStringProperty("0");
  public StringProperty minutesMinor = new SimpleStringProperty("0");
  public StringProperty secondsMajor = new SimpleStringProperty("0");
  public StringProperty secondsMinor = new SimpleStringProperty("0");

  public StringBinding timeLapse = new StringBinding() {

    {
      super.bind(daysMajor, daysMinor, hoursMajor, hoursMinor, minutesMajor, minutesMinor, secondsMajor, secondsMinor);
    }

    @Override
    protected String computeValue() {
      return daysMajor.getValue() + daysMinor.getValue() + ":"
          + hoursMajor.getValue() + hoursMinor.getValue() + ":"
          + minutesMajor.getValue() + minutesMinor.getValue() + ":"
          + secondsMajor.getValue() + secondsMinor.getValue();
    }

  };

  private final Date submitDate;

  public TimerDisplayModel(Date submitDate) {
    this.submitDate = submitDate;
  }

  @Override
  public void onTick() {
    int passed = new Double(Math.floor((Calendar.getInstance().getTimeInMillis() - submitDate.getTime()) / 1000)).intValue();

    // Number of days passed
    int d = new Double(Math.floor(passed / DAYS)).intValue();
    updateDays(d);
    passed -= d * DAYS;

    // Number of hours left
    int h = new Double(Math.floor(passed / HOURS)).intValue();
    updateHours(h);
    passed -= h * HOURS;

    // Number of minutes left
    int m = new Double(Math.floor(passed / MINUTES)).intValue();
    updateMinutes(m);
    passed -= m * MINUTES;

    // Number of seconds left
    int s = passed;
    updateSeconds(s);

  }

  private void updateDays(int value) {
    daysMajor.set(String.valueOf(getMajor(value)));
    daysMinor.set(String.valueOf(getMinor(value)));
  }

  private void updateHours(int value) {
    hoursMajor.set(String.valueOf(getMajor(value)));
    hoursMinor.set(String.valueOf(getMinor(value)));
  }

  private void updateMinutes(int value) {
    minutesMajor.set(String.valueOf(getMajor(value)));
    minutesMinor.set(String.valueOf(getMinor(value)));
  }

  private void updateSeconds(int value) {
    secondsMajor.set(String.valueOf(getMajor(value)));
    secondsMinor.set(String.valueOf(getMinor(value)));
  }

  private Integer getMajor(int value) {
    return new Double(Math.floor(value / 10) % 10).intValue();
  }

  private Integer getMinor(int value) {
    return new Double(value % 10).intValue();
  }
}
