/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.versatiledisplayer.display.helpdesk;

import javafx.scene.control.TableCell;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public class WHelpDeskCaseCell extends TableCell<HelpDeskCaseProxy, String> {

    private Text text;

    @Override
    protected void updateItem(String item, boolean empty) {
        this.setStyle("-fx-font-weight: bold; -fx-padding: 5pt;");
        super.updateItem(item, empty);
        if (!isEmpty()) {
            text = new Text(item);
//            text.setWrappingWidth(MainApp.SCREEN_WIDTH * 69 / 100);
            text.setFill(Color.WHITE);
            setGraphic(text);
        }
    }

}
