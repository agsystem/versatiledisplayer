/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.versatiledisplayer.display.helpdesk;

import javafx.scene.control.TableCell;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import mdn.versatiledisplayer.Displayer;

/**
 *
 * @author mbahbejo
 */
public class HelpDeskCaseCenteredCell extends TableCell<HelpDeskCaseProxy, String> {
  private Text text;

    @Override
    protected void updateItem(String item, boolean empty) {
        this.setStyle("-fx-font-weight: bold; -fx-padding: 5pt;");
        super.updateItem(item, empty);
        textProperty().setValue(item);
        if (!isEmpty()) {
            text = new Text(item);
            text.setFill(Color.WHITE);
            text.setWrappingWidth(Displayer.SCREEN_WIDTH * 15 / 100);
            setGraphic(text);
        }
    }

}
