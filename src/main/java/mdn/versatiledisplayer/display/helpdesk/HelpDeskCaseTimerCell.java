/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.versatiledisplayer.display.helpdesk;

import java.util.Date;
import javafx.scene.control.TableCell;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public class HelpDeskCaseTimerCell extends TableCell<HelpDeskCaseProxy, Date> {

    @Override
    protected void updateItem(Date item, boolean empty) {
        this.setStyle("-fx-font-weight: bold; -fx-padding: 5pt;");
        super.updateItem(item, empty);
        if (item == null) {
            return;
        }
        TimerDisplayModel model = new TimerDisplayModel(item);
        MasterTimer.getInstance().addListener(model);
        textProperty().bind(model.timeLapse);
    }

}
