/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.versatiledisplayer.display.helpdesk;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javafx.application.Platform;
import javax.swing.Timer;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public class MasterTimer {

  private static MasterTimer instance;

  private final Timer timer;
  private List<TimerListener> timerListeners;

  private MasterTimer() {
    timerListeners = new ArrayList<>();
    timer = new Timer(1000, new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            for (TimerListener timerListener : timerListeners) {
              timerListener.onTick();
            }
          }
        });
      }
    });
  }

  public static MasterTimer getInstance() {

    if (instance == null) {
      instance = new MasterTimer();
    }

    return instance;

  }

  public void start() {
    timer.start();
  }

  public void stop() {
    timer.stop();
  }

  public void addListener(TimerListener listener) {
    timerListeners.add(listener);
  }

  public void removeListener(TimerListener listener) {
    timerListeners.remove(listener);
  }

  public void clearListener() {
    timerListeners.clear();
  }

}
