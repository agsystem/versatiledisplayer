/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.versatiledisplayer.display.linecounter;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import mdn.versatiledisplayer.Displayer;

/**
 * FXML Controller class
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public class WorkUnitController extends AnchorPane implements Initializable {

    @FXML
    private Label lblWorkUnit;

    @FXML
    private Label lblQC;

    @FXML
    private Label lblPos1;

    @FXML
    private Label lblPos2;

    @FXML
    private Label lblPos3;

    @FXML
    private Label lblPos4;

    @FXML
    private Label lblPos5;

    @FXML
    private Label lblPos6;

    @FXML
    private Label lblPos7;

    @FXML
    private Label lblPos8;

    @FXML
    private Label lblPos9;

    @FXML
    private Label lblPos10;

    @FXML
    private Label lblPos11;

    @FXML
    private Label lblPos12;

    @FXML
    private Label lblPos13;

    @FXML
    private Label lblPos14;

    @FXML
    private Label lblPos15;

    @FXML
    private Label lblPos16;

    @FXML
    private Label lblPos17;

    @FXML
    private Label lblPos18;

    @FXML
    private Label lblPos19;

    @FXML
    private Label lblPos20;

    @FXML
    private Label lblPos21;

    @FXML
    private Label lblPos22;

    @FXML
    private Label lblPos23;

    @FXML
    private Label lblPos24;

    @FXML
    private Label lblPos25;

    @FXML
    private Label lblPos26;

    @FXML
    private Label lblPos27;

    @FXML
    private Label lblPos28;

    @FXML
    private Label lblPos29;

    @FXML
    private Label lblPos30;

    @FXML
    private Label lblPos31;

    @FXML
    private Label lblPos32;

    @FXML
    private Label lblPos33;

    @FXML
    private Label lblPos34;

    @FXML
    private Label lblPos35;

    @FXML
    private Label lblPos36;

    @FXML
    private Label lblPos37;

    @FXML
    private Label lblPos38;

    @FXML
    private Label lblPos39;

    @FXML
    private Label lblPos40;

    @FXML
    private Label lblPos41;

    @FXML
    private Label lblPos42;

    @FXML
    private Label lblPos43;

    @FXML
    private Label lblPos44;

    @FXML
    private Label lblPos45;

    @FXML
    private Label lblPos46;

    private Map<String, Label> positions = new HashMap<>();

    public WorkUnitController() {
        final FXMLLoader loader = new FXMLLoader(Displayer.class.getResource("/fxml/linecounter/WorkUnit.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException ex) {
            Logger.getLogger(WorkUnitController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setLabel(final String label) {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                lblWorkUnit.textProperty().set(label);
            }
        });
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        positions.put("0", lblQC);

        positions.put("1", lblPos1);

        positions.put("2", lblPos2);

        positions.put("3", lblPos3);

        positions.put("4", lblPos4);

        positions.put("5", lblPos5);

        positions.put("6", lblPos6);

        positions.put("7", lblPos7);

        positions.put("8", lblPos8);

        positions.put("9", lblPos9);

        positions.put("10", lblPos10);

        positions.put("11", lblPos11);

        positions.put("12", lblPos12);

        positions.put("13", lblPos13);

        positions.put("14", lblPos14);

        positions.put("15", lblPos15);

        positions.put("16", lblPos16);

        positions.put("17", lblPos17);

        positions.put("18", lblPos18);

        positions.put("19", lblPos19);

        positions.put("20", lblPos20);

        positions.put("21", lblPos21);

        positions.put("22", lblPos22);

        positions.put("24", lblPos24);

        positions.put("25", lblPos25);

        positions.put("26", lblPos26);

        positions.put("27", lblPos27);

        positions.put("28", lblPos28);

        positions.put("29", lblPos29);

        positions.put("30", lblPos30);

        positions.put("31", lblPos31);

        positions.put("32", lblPos32);

        positions.put("33", lblPos33);

        positions.put("34", lblPos34);

        positions.put("35", lblPos35);

        positions.put("36", lblPos36);

        positions.put("37", lblPos37);

        positions.put("38", lblPos38);

        positions.put("39", lblPos39);

        positions.put("40", lblPos40);

        positions.put("41", lblPos41);

        positions.put("42", lblPos42);

        positions.put("43", lblPos43);

        positions.put("44", lblPos44);

        positions.put("45", lblPos45);

        positions.forEach((key, value) -> {
            value.getStyleClass().add("counter");
        });

        positions.put("23", lblPos23);
        lblPos23.getStyleClass().add("counter-last");

        positions.put("46", lblPos46);
        lblPos46.getStyleClass().add("counter-last");

        lblWorkUnit.getStyleClass().add("workunit");
    }

    public void inc(String strButton, final String strLabel) {
        final Label lbl = positions.get(strButton);
        if (lbl != null) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    lbl.textProperty().set(strLabel);
                }
            });
        }
    }

    synchronized public void updateState(String strButton, final String status, final String lastCount) {
        final Label lbl = positions.get(strButton);
        if (lbl != null) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    lbl.textProperty().set(lastCount);
                    if(status == null) return;
                    if (status.equals("counter-used")) {
                        lbl.getStyleClass().remove("counter-unused");
                        lbl.getStyleClass().remove("counter-error");
                        lbl.getStyleClass().add(status);
                    } else if(status.equals("counter-unused")) {
                        lbl.getStyleClass().remove("counter-used");
                        lbl.getStyleClass().remove("counter-error");
                        lbl.getStyleClass().add(status);
                    } else {
                        lbl.getStyleClass().remove("counter-unused");
                        lbl.getStyleClass().remove("counter-used");
                        lbl.getStyleClass().add(status);                        
                    }
                }
            });
        }
    }

}
