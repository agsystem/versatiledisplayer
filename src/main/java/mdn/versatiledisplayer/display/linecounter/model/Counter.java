/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.versatiledisplayer.display.linecounter.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public class Counter {
    
    private CounterPosition counterPosition;
    
    private final IntegerProperty count = new SimpleIntegerProperty(this, "count", 0);

    public Counter() {
        this(0);
    }
    
    public Counter(int count) {
        this.count.set(count);
    }

    public CounterPosition getCounterPosition() {
        return counterPosition;
    }

    public void setCounterPosition(CounterPosition counterPosition) {
        this.counterPosition = counterPosition;
    }

    public final IntegerProperty countProperty() {
        return count;
    }
    
    public int getCount() {
        return countProperty().get();
    }

    public void setCount(int count) {
        countProperty().set(count);
    }
    
}
