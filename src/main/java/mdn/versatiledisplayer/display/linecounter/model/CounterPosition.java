/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.versatiledisplayer.display.linecounter.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public class CounterPosition {
    
    private final WorkUnit workUnit;
    
    private final IntegerProperty num = new SimpleIntegerProperty(this, "num", 0);
    
    private Counter counter;

    public CounterPosition() {
        this(null, 0);
    }
    
    public CounterPosition(WorkUnit workUnit, int num) {
        this.workUnit = workUnit;
        this.num.set(num);
    }

    public WorkUnit getWorkUnit() {
        return workUnit;
    }

    public int getNum() {
        return numProperty().get();
    }

    public void setNum(int num) {
        numProperty().set(num);
    }
    
    public final IntegerProperty numProperty() {
        return num;
    }

    public Counter getCounter() {
        return counter;
    }

    public void setCounter(Counter counter) {
        this.counter = counter;
    }
    
}
