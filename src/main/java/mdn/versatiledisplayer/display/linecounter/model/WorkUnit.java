/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.versatiledisplayer.display.linecounter.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public class WorkUnit {
    
    private final StringProperty id = new SimpleStringProperty(this, "id", null);
    
    private final StringProperty name = new SimpleStringProperty(this, "name", null);
    
    private final ObservableList<CounterPosition> counterPositions = FXCollections.observableArrayList();

    public WorkUnit() {
        this(null, null);
    }

    public WorkUnit(String id, String name) {
        this.name.set(name);
    }
    
    public final StringProperty idProperty() {
        return id;
    }
    
    public String getId() {
        return idProperty().get();
    }
    
    public void setId(String id) {
        idProperty().set(id);
    }

    public String getName() {
        return name.get();
    }
 
    public void setName(String name) {
        nameProperty().set(name);
    }
    
    public final StringProperty nameProperty() {
        return name;
    }

    public ObservableList<CounterPosition> getCounterPositions() {
        return counterPositions;
    }
    
}
