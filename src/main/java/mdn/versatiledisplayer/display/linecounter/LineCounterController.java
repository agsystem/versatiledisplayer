package mdn.versatiledisplayer.display.linecounter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import javafx.fxml.FXML;
import javafx.scene.layout.VBox;
import mdn.iotlib.comm.message.Message;
import mdn.versatiledisplayer.common.Device;
import mdn.versatiledisplayer.common.DisplayController;

public class LineCounterController implements DisplayController {

    @FXML
    private VBox vBox;

    private Device device;

    private final ObservableMap<String, WorkUnitController> workUnitControllers = FXCollections.observableHashMap();

    public LineCounterController() {

    }

    @FXML
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    @Override
    public String getDisplayName() {
        return "LineCounter";
    }

    @Override
    public void process(final Message msg) {
        Gson gson = new Gson();
        Type collectionType = new TypeToken<Map<String, String>>() {}.getType();
        Map<String, String> params = gson.fromJson(msg.getString(), collectionType);
//        String cmd = msg.getString("command");
        switch (params.get("type")) {
            case "counting": {
                String id = params.get("workUnit");
                String strButton = params.get("positionNumber");
                String strLabel = params.get("lastCount");
                WorkUnitController wuc = workUnitControllers.get(id);
                if (wuc != null) {
                    wuc.inc(strButton, strLabel);
                }
            }
            break;
//            case "paired": {
//                String id = msg.getString("workUnit");
//                String strButton = msg.getString("position");
//                String lastCount = msg.getString("lastCount");
//                String status = msg.getString("status");
//                WorkUnitController foundController = workUnitControllers.get(id);
//                if (foundController != null) {
//                    foundController.updateState(strButton, status, lastCount);
//                }
//            }
//            break;
//            case "error": {
//                String id = msg.getString("workUnit");
//                String strButton = msg.getString("position");
//                String lastCount = msg.getString("lastCount");
//                String status = msg.getString("status");
//                WorkUnitController foundController = workUnitControllers.get(id);
//                if (foundController != null) {
//                    foundController.updateState(strButton, status, lastCount);
//                }
//            }
//            break;
            case "sync-unit": {
                WorkUnitController newController = new WorkUnitController();
                newController.setLabel(params.get("label"));
                String id = params.get("id");
                workUnitControllers.put(id, newController);
                Platform.runLater(() -> {
                    vBox.getChildren().add(newController);
                });
            }
            break;
            case "sync-position": {
                String id = params.get("workUnit");
                String strButton = params.get("positionNumber");
                String lastCount = params.get("lastCount");
                String status = params.get("status");
                System.out.println("SINKRON " + strButton + " STATUS " + status);
                WorkUnitController foundController = workUnitControllers.get(id);
                if (foundController != null) {
                    foundController.updateState(strButton, status, lastCount);
                }
            }
//            break;
//            case "reset":
//                String id = msg.getString("workUnit");
//                String strButton = msg.getString("position");
//                String lastCount = String.valueOf(0);
//                WorkUnitController foundController = workUnitControllers.get(id);
//                if (foundController != null) {
//                    foundController.updateState(strButton, null, lastCount);
//                }
//                break;
        }
    }

    @Override
    public void onOpen() {
        Message msg = new Message(getDisplayName().getBytes().length);
        msg.setSource(getDevice().getId());
        msg.setFlag(-1);
        msg.putString(getDisplayName());
        try {
            getDevice().send(msg);
        } catch (Exception ex) {
            Logger.getLogger(LineCounterController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void setDevice(Device device) {
        this.device = device;
    }

    @Override
    public Device getDevice() {
        return device;
    }

    @Override
    public void onClose() {

    }

    @Override
    public void onHide() {

    }

}
