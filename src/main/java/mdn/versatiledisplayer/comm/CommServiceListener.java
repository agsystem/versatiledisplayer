/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.versatiledisplayer.comm;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public interface CommServiceListener {

    public void onOpen();

    public void onClose();
}
