/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.linecounter.comm.exception;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public class CommServiceSendingException extends Exception {

    public CommServiceSendingException() {
        super("Send error.");
    }

    public CommServiceSendingException(Throwable cause) {
        super("Send error.", cause);
    }
    
}
