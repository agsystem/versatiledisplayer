/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.linecounter.comm.exception;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public class CommServiceConnectionException extends Exception {

    public CommServiceConnectionException() {
        super("Connection failed.");
    }

    public CommServiceConnectionException(Throwable cause) {
        super("Connection failed.", cause);
    }
    
}
