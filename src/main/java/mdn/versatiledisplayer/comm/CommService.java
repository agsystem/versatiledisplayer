/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.versatiledisplayer.comm;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.ClientEndpoint;
import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;
import mdn.iotlib.comm.message.Message;
import mdn.iotlib.comm.message.MessageDecoder;
import mdn.iotlib.comm.message.MessageEncoder;
import mdn.iotlib.comm.message.MessageListener;
import mdn.versatiledisplayer.common.VersatileDisplayerDevice;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@ClientEndpoint(
        encoders = {MessageEncoder.class},
        decoders = {MessageDecoder.class}
)
public class CommService {

    private VersatileDisplayerDevice hostDevice;

    private final URI serverAddress;

    private Session dataSession;

    final List<MessageListener> listeners = new ArrayList<>();

    final CommService that;

    private ExecutorService connectExecutor;

    private ExecutorService queueExecutor;

    private volatile boolean trying = true;

    public CommService(VersatileDisplayerDevice hostDevice, URI serverAddress) {
        this.hostDevice = hostDevice;
        this.serverAddress = serverAddress;
        that = this;

    }

    private final Runnable runConnector = new Runnable() {

        @Override
        public void run() {

            System.out.println("Trying to connect... ");

            Session session = null;

            while (trying) {
                WebSocketContainer container = ContainerProvider.getWebSocketContainer();
                try {

                    session = container.connectToServer(that, serverAddress);

                } catch (IOException ex) {
                    Logger.getLogger(CommService.class.getName()).log(Level.SEVERE, null, ex);
                    continue;
                } catch (DeploymentException ex) {

                }
            }
        }

    };

    private ThreadFactory daemonThreadFactory = new ThreadFactory() {

        @Override
        public Thread newThread(Runnable r) {
            Thread t = new Thread(r);
            t.setDaemon(true);
            return t;
        }

    };

    public void connect() {
        try {
            connectExecutor = Executors.newSingleThreadExecutor(daemonThreadFactory);
            connectExecutor.submit(runConnector);
            connectExecutor.shutdown();
        } catch (Exception ex) {
            Logger.getLogger(CommService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @OnOpen
    public void onOpen(Session session) {

        trying = false;

        dataSession = session;

        addListener(hostDevice);

        if (!hostDevice.isRegistered()) {

            hostDevice.register();
        }

        queueExecutor = Executors.newSingleThreadExecutor(daemonThreadFactory);

        queueExecutor.submit(hostDevice.getRunnableQueue());

        queueExecutor.shutdown();

        connectExecutor.shutdownNow();

        System.out.println("Connected..");

    }

    @OnMessage
    public void onMessage(Message message) {
        notifyListeners(message);
    }

    @OnError
    public void onError(Throwable t) {
        Logger.getLogger(CommService.class
                .getName()).log(Level.SEVERE, null, t);
    }

    @OnClose
    public void onClose(Session session) {
        System.out.println("CLOSED");
        if (!trying) {
            queueExecutor.shutdownNow();
            trying = true;
            connect();

        }
    }

    public void send(Message msg) throws IOException, EncodeException {
        if (dataSession != null && dataSession.isOpen()) {
            dataSession.getBasicRemote().sendObject(msg);
        }
    }

    public void addListener(MessageListener listener) {
        listeners.add(listener);
    }

    public void removeListener(MessageListener listener) {
        listeners.remove(listener);
    }

    public void notifyListeners(Message msg) {
        for (MessageListener listener : listeners) {
            listener.onMessage(msg);
        }
    }

}
