/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.versatiledisplayer;

import java.net.URL;
import java.util.ResourceBundle;
import mdn.iotlib.comm.message.Message;
import mdn.versatiledisplayer.common.Device;
import mdn.versatiledisplayer.common.DisplayController;

/**
 * FXML Controller class
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public class HomeController  implements DisplayController {
    
    private Device device;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    } 

    @Override
    public String getDisplayName() {
        return "Home";
    }

    @Override
    public void process(Message msg) {
        
    }

    @Override
    public void setDevice(Device device) {
        this.device = device;
    }

    @Override
    public Device getDevice() {
        return device;
    }

    @Override
    public void onOpen() {
        
    }

    @Override
    public void onClose() {
        
    }

    @Override
    public void onHide() {
        
    }
    
}
