/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.versatiledisplayer.common;

import javafx.fxml.Initializable;

/**
 *
 * @author mbahbejo
 */
public interface DisplayController extends Initializable, DeviceListener {
    public String getDisplayName();
    public void setDevice(Device device);
    public Device getDevice();
    public void onOpen();
    public void onHide();
    public void onClose();
}
