/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.versatiledisplayer.common;

import java.io.IOException;
import java.util.UUID;
import javax.websocket.EncodeException;
import mdn.iotlib.comm.message.Message;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public interface Device {
    public UUID getId();
    public void send(Message message) throws IOException, EncodeException;
    public void receive(Message message);
    public void stop() throws Exception;
}
