/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.versatiledisplayer.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.List;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javax.websocket.EncodeException;
import mdn.iotlib.comm.message.Message;
import mdn.iotlib.comm.message.MessageListener;
import mdn.versatiledisplayer.Displayer;
import mdn.versatiledisplayer.comm.CommService;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public class VersatileDisplayerDevice implements Device, MessageListener {

    private final UUID id;

    private DeviceListener listener;

    private final Stage stage;

    private double width;

    private double height;

    private DisplayController activeController;

    private CommService commService;

    private Queue<Message> queue;

    private volatile boolean registered;

    private final Runnable runnableQueue = new Runnable() {

        @Override
        public void run() {

            while (true) {

                try {

                    Message msg = queue.peek(); // Look at the head of queue.

                    if (msg != null) { // If found a message waiting to be sent.
                        System.out.println("SENDING MESSAGE...");
                        tryToSend(msg); // Blocked until successfully sent the message.
                        System.out.println("Message sent.");

                        queue.poll(); // Remove the message that has been sent.

                    } else { // If queue is empty.
                        System.out.println("Entering wait state...");
                        synchronized (this) {
                            wait();
                        }
                        System.out.println("Leaving wait state.");
                    }

                } catch (InterruptedException ex) {
                    break;
                } catch (IOException | EncodeException ex) {
                    Logger.getLogger(CommService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        public void tryToSend(Message msg) throws IOException, EncodeException {

            send(msg);

        }

    };

    public VersatileDisplayerDevice(Stage stage) {
        this.stage = stage;
        id = UUID.fromString(uuid());
    }

    public void boot() {

        setup();

        initCommService();
    }

    private void setup() {
        stage.setTitle("Versatile Displayer");
        List<Screen> screens = Screen.getScreens();
        Screen screen = screens.get(screens.size() - 1);
        Rectangle2D rect = screen.getBounds();
        width = rect.getWidth();
        height = rect.getHeight();
        stage.setX(rect.getMinX());
        stage.setY(rect.getMinY());
        stage.centerOnScreen();
        stage.setFullScreen(true);

        stage.setOnShown(new EventHandler<WindowEvent>() {

            @Override
            public void handle(WindowEvent event) {
                if (activeController != null) {
                    activeController.onOpen();
                }
            }

        });

        stage.setOnHidden(new EventHandler<WindowEvent>() {

            @Override
            public void handle(WindowEvent event) {
                if (activeController != null) {
                    activeController.onHide();
                }
            }

        });

        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {

            @Override
            public void handle(WindowEvent event) {
                if (activeController != null) {
                    activeController.onClose();
                }
            }

        });

        queue = new ConcurrentLinkedQueue();
    }

    private void initCommService() {

        try {
            String serverAddress = Displayer.getInstance().getParameter("server-address", "localhost");
            String serverPort = Displayer.getInstance().getParameter("server-port", "8080");
            commService = new CommService(this, new URI("ws://" + serverAddress + ":" + serverPort + "/tiger/websocket/versatiledisplayer/" + id));
            commService.connect();
        } catch (URISyntaxException ex) {
            Logger.getLogger(VersatileDisplayerDevice.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void register() {
        String payload = getIpAddr() + "|" + getHostName();
        Message registerMessage = new Message(payload.getBytes().length);
        registerMessage.setSource(id);
        registerMessage.setFlag(0);
        registerMessage.putString(payload);

        try {
            send(registerMessage);
            registered = true;
        } catch (IOException ex) {
            Logger.getLogger(VersatileDisplayerDevice.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EncodeException ex) {
            Logger.getLogger(VersatileDisplayerDevice.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Device getDevice() {
        return this;
    }

    private void show(final String xml) throws IOException {

        final FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/" + xml + ".fxml"));

        Parent root = fxmlLoader.load();
        activeController = fxmlLoader.<DisplayController>getController();
        activeController.setDevice(getDevice());
        listener = activeController;

        final Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/" + xml + ".css");

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                stage.hide();
                stage.setScene(scene);
                stage.show();
            }

        });

    }

    @Override
    public void receive(Message message) {
        int flag = message.getFlag();
        switch (flag) {
            case 1:
                try {
                    show(message.getString());
                } catch (IOException ex) {
                    Logger.getLogger(VersatileDisplayerDevice.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case -1:
                listener.process(message);
                break;
        }
    }

    @Override
    public void onMessage(Message msg) {
        receive(msg);
//        String cmd = msg.getString("command");
//        switch (cmd) {
//            case "ready":
//                String xml = msg.getString("default");
//                try {
//                    if (xml != null) {
//                        show(xml);
//                    } else {
//                        show("home");
//                    }
//                } catch (Exception ex) {
//                    Logger.getLogger(Displayer.class.getName()).log(Level.SEVERE, null, ex);
//                }
//                break;
//            case "set-default":
//                break;
//            case "change-display":
//                try {
//                    activeController.onHide();
//                    show(msg.getString("display"));
//                } catch (IOException ex) {
//                    Logger.getLogger(Displayer.class.getName()).log(Level.SEVERE, null, ex);
//                }
//                break;
//            default:
//                super.receive(msg);
//                break;
//        }
    }

    //<editor-fold defaultstate="collapsed" desc="Getter & Setter">
    public Stage getStage() {
        return stage;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public DisplayController getActiveController() {
        return activeController;
    }
//</editor-fold>

    @Override
    public void stop() throws Exception {

    }

    @Override
    public void send(Message message) throws IOException, EncodeException {
        commService.send(message);
    }

    public String getAppName() {
        return "VersatileDisplayer";
    }

    protected String getIpAddr() {
        String strAddr = "";
        try {
            Enumeration<NetworkInterface> ifaces = NetworkInterface.getNetworkInterfaces();
            while (ifaces.hasMoreElements()) {
                NetworkInterface iface = ifaces.nextElement();
                if (iface.isUp() && !iface.isLoopback() && !iface.isVirtual() && !iface.isPointToPoint()) {
                    Enumeration<InetAddress> addrs = iface.getInetAddresses();
                    while (addrs.hasMoreElements()) {
                        InetAddress addr = addrs.nextElement();
                        if (addr.isSiteLocalAddress()) {
                            strAddr = addr.getHostAddress();
                        }
                    }
                }
            }

            return strAddr;
        } catch (SocketException ex) {
            Logger.getLogger(VersatileDisplayerDevice.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return strAddr;
        }
    }

    protected String getHostName() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException ex) {
            return "";
        }
    }

    public String uuid() {

        File configDir = Displayer.getInstance().getConfigDir();
        File uuidFile = new File(configDir, "uuid");
        String uuid = "";

        if (uuidFile.exists()) {

            try (FileReader fread = new FileReader(uuidFile)) {
                int count;
                char[] buff = new char[16];
                do {
                    count = fread.read(buff);
                    for (int i = 0; i < count; i++) {
                        uuid += buff[i];
                    }
                } while (count != -1);

                fread.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(VersatileDisplayerDevice.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(VersatileDisplayerDevice.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            uuid = UUID.randomUUID().toString().trim();

            try (FileWriter fwriter = new FileWriter(uuidFile)) {
                fwriter.write(uuid, 0, uuid.length());

                fwriter.close();
            } catch (IOException ex) {
                Logger.getLogger(VersatileDisplayerDevice.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return uuid;

    }

    @Override
    public UUID getId() {
        return id;
    }

    public Queue<Message> getQueue() {
        return queue;
    }

    public boolean isRegistered() {
        return registered;
    }

    public Runnable getRunnableQueue() {
        return runnableQueue;
    }

    public void queued(Message msg) {
        getQueue().offer(msg);
        synchronized (runnableQueue) {
            runnableQueue.notify();
        }
    }

}
