/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.versatiledisplayer;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.stage.Stage;
import mdn.versatiledisplayer.common.VersatileDisplayerDevice;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public class Displayer extends Application {

    public static double SCREEN_WIDTH;
    public static double SCREEN_HEIGHT;

    private static Displayer instance;

    static {
        SCREEN_WIDTH = javafx.stage.Screen.getPrimary().getVisualBounds().getWidth() - 5;
        SCREEN_HEIGHT = javafx.stage.Screen.getPrimary().getVisualBounds().getHeight() + 25;
    }

    public static Displayer getInstance() {
        return instance;
    }

    private Parameter parameter;

    @Override
    public void init() throws Exception {
        super.init();
        parameter = new Parameter(getParameters().getRaw().toArray(new String[getParameters().getRaw().size()]));
        instance = this;
    }

    @Override
    public void start(Stage stage) {

        VersatileDisplayerDevice device = new VersatileDisplayerDevice(stage);

        device.boot();

    }

    @Override
    public void stop() throws Exception {
        super.stop();
    }

    public static void main(String[] args) {
        launch(args);
    }

    public String getName() {
        return "VersatileDisplayer";
    }

    public File getConfigDir() {

        String userdir = System.getProperty("user.home");
        File dir = new File(userdir, "." + getName());
        if (!dir.exists()) {
            dir.mkdir();
        }

        return dir;

    }

    public String getParameter(String name) {
        return parameter.getParameters().get(name);
    }

    public String getParameter(String name, String defaultValue) {
        return parameter.getParameters().getOrDefault(name, defaultValue);
    }

    private void setParameter(Parameter parameter) {
        this.parameter = parameter;
    }

    private final static class Parameter {

        private final Map<String, String> parameters = new HashMap<>();

        private Parameter(String... args) {
            for (String arg : args) {
                String[] pair = arg.split("=");
                parameters.put(pair[0].replaceFirst("--", ""), pair[1]);
            }
        }

        public final Map<String, String> getParameters() {
            return parameters;
        }

    }

}
