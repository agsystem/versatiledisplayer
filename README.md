# README #

This project was designed to support the main app (https://bitbucket.org/agsystem/tiger) to display some informations to public in large format display.

### How do I get set up? ###

1. Compile the project.
2. Run: java -jar VersatileDisplayer.jar  --server-address=ip_address_of_the_computer_running_main_app --server-port=port_used_by_the_computer_running_main_app
3. Done.
